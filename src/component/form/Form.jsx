import { useState } from 'react'
import styles from './form.module.css'
import DeleteButton from "../delete-button/DeleteButton"

const Form = ({ handleTodos, todos }) => {
    const [error, setError] = useState(false);
    const [inputValue, setInputValue] = useState('');


    const handleChange = (e) => {
        setInputValue(e.target.value)
    }

    const onClear = () => {
        setInputValue('')
    }

    const onSubmit = (e) => {
        let value = inputValue.trim()
        if (value && value.length <= 54) {
            const newTodo = {
                id: Math.random(),
                value,
                checked: false
            }
            const todoList = [...todos]
            todoList.push(newTodo)
            handleTodos(todoList)
            setInputValue('')
            if (error) {
                setError('')
            }
        } else if (!value || value.length > 54) {
            setError(true)
        }

    }


    return (
        <div className={styles.formContainer}>
            <div className={styles.formItems}>
                <div className={styles.inputWrapper}>
                    <label htmlFor="taskInput" className={styles.task}>Task</label>
                    <div className={error ? styles.errorInput : ''}>
                        <input
                            id="taskInput"
                            className={styles.addInput}
                            name="task"
                            placeholder="Write Here"
                            type='text'
                            value={inputValue}
                            onChange={handleChange} />
                        {inputValue && <DeleteButton onClick={onClear} className={styles.clearButton} />}
                    </div>
                </div>
                  <button type="button" onClick={onSubmit} className={styles.addButton}>Add</button>
            </div>

            {error ? <p className={styles.errorText}>Task content can contain max 54 characters.</p> : ""}

        </div>
    )
}

export default Form
