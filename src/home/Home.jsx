import styles from "./Home.module.css"
import TodoList from "../component/todo-list/TodoList"
import EmptyState from "../component/empty-state/EmptyState"
import DeleteModal from "../component/delete-modal/DeleteModal"
import { useState } from "react"
import { useEffect } from "react"
import HideCompleted from "../component/hide-completed/HideCompleted"
import Form from "../component/form/Form"

const Home = () => {

    const [todos, setTodos] = useState([]);

    const [openModal, setOpenModal] = useState({
        isOpen: false,
        id: ""
    })

    const handleModal = (isOpen, id) => {
        setOpenModal({isOpen, id})
    }

    const handleTodos = (todos) => {
        setTodos(todos)
        localStorage.setItem("todoList", JSON.stringify(todos))

    }

  
    useEffect(()=>{
        const todoList = JSON.parse(localStorage.getItem('todoList')) 
        if(todoList) setTodos(todoList)
  
      }, [])

    return (
        <div className={styles.home}>
            <div className={styles.homePage}>
            <HideCompleted todos={todos} handleTodos={handleTodos}/>
            <Form handleTodos={handleTodos} todos={todos}/>
            
            {todos.length ? <TodoList todos={todos} handleModal={handleModal} handleTodos={handleTodos}  /> : <EmptyState />}
            <DeleteModal open={openModal} handleModal={handleModal} handleTodos={handleTodos} todos={todos}/>
            </div>
        </div>
    )
}

export default Home