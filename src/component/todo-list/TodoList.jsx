import DeleteButton from "../delete-button/DeleteButton"
import styles from "./TodoList.module.css"

const TodoList = ({ todos, handleModal, handleTodos }) => {
    const handleCheck = (e, id) => {
        const { checked } = e.target
        const newTodos = [...todos]
        const index = newTodos.findIndex(item => item.id === id)
        if (index !== -1) {
            newTodos[index] = { ...newTodos[index], checked }
            handleTodos(newTodos)

        }
    }

    return (
        <div className={styles.todo}>
            {
                todos.map(({ id, value, checked }) => {
                    return (
                        <div key={id} className={styles.todoList} >

                            <div className={styles.list}>
                                <input onChange={(e) => handleCheck(e, id)}
                                    checked={checked}
                                    className={styles.checkbox}
                                    type="checkbox" />
                                <p className={`${styles.text} ${checked ? styles.checked : ''}`}>{value}</p>
                            </div>

                            <DeleteButton className={styles.deletButton} onClick={() => handleModal(true, id)} />
                        </div>

                    )
                })
            }
        </div>

    )
}

export default TodoList

