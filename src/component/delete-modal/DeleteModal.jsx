import styles from "./DeleteModal.module.css"

const DeleteModal = ({open, handleModal, handleTodos, todos}) => {
    const onDelete = (id) => {
        const filteredTodos = todos.filter((item) => item.id !== id )
        handleTodos(filteredTodos)
        handleModal(false, '')
  
      }
    return(
       open.isOpen &&
        <div className={styles.backdrop}>
        <div className={styles.deleteModal}>
        <p>Are you sure you want to delete?</p>
             <button onClick={()=>onDelete(open.id)}>Yes</button>
             <button onClick={()=>handleModal(false, '')}>No</button>
         </div>
        </div>

    )
}

export default DeleteModal