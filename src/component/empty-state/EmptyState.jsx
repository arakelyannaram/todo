import styles from "./EmptyState.module.css"

const EmptyState = () => {
    return(
        <div className={styles.emptyState}>
            <p>Your life is a blank page.You write on it. </p>
            <p>So start by adding your tasks here.</p>
        </div>
    )
}

export default EmptyState