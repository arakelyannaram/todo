import styles from './HideCompleted.module.css'

const HideCompleted = ({ todos, handleTodos }) => {

    const hideCompleted = (e) => {
        const { checked } = e.target
        if (checked) {
            const filtered = todos.filter((item) => item.checked === false)
            handleTodos(filtered)
        } else {
            const items = JSON.parse(localStorage.getItem('todoList'))
            handleTodos(items)
        }

    }

    return (
        todos.length ?
            <div className={styles.completedCheckbox}>
                <input type="checkbox" id="completedCheckbox" className={styles.checkbox} onChange={hideCompleted} />
                <label htmlFor="completedCheckbox" className={styles.completed}>Hide Completed</label>
            </div> : null
    )
}

export default HideCompleted
